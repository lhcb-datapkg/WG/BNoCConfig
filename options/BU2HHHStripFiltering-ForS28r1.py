"""
Stripping Filtering file for Chris Thomas
@author Valdir Salustino
@date   2017-08-29
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"


#To obtain the rejection factor, can comment. 
#from Configurables import DumpFSR, DaVinci
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]


#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)


#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping28r1'
config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)


def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

_filterlines = quickBuild('Bhadron')

MyStream = StrippingStream("B2HHH.Strip")
MyLines = ['StrippingBu2hhh_KKK_inclLine','StrippingBu2hhh_pph_inclLine','StrippingXb2phhLine','StrippingBu2hhh_KKK_samesignLine']

for line in _filterlines.lines :
    if line.name() in MyLines:
        MyStream.appendLines( [ line ] )

for line in _filterlines.lines:
   line._prescale = 1.0
   
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements,
                                              stripMicroDSTStreamConf,
                                              stripMicroDSTElements,
                                              stripCalibMicroDSTStreamConf
                                              )

# Configuration of MicroDST
# per-event an per-line selective writing of the raw event is active (selectiveRawEvent=True)
#mdstStreamConf = stripMicroDSTStreamConf(pack='enablePacking', selectiveRawEvent=False)
#mdstElements   = stripMicroDSTElements(pack='enablePacking')

enablePacking = True

SelDSTWriterElements = {
    'default'              : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'              : stripDSTStreamConf(pack=enablePacking)
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41442810)



from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1 
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

