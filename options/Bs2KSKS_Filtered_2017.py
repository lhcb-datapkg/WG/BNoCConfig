"""Stripping filtering script for Bs -> (KS -> pi+ pi-) (KS -> pi+ pi-)
Year: 2017
Stripping version: S29r2
DaVinci version: v42r7p2
@author Kerim Guseinov
@date   2023-10-09
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"


stripping = 'stripping29r2'

# Use particle definitions from the archive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

# Raw event juggler to split Other/RawEvent into Velo/RawEvent and
# Tracker/RawEvent
from Configurables import RawEventJuggler
juggler = RawEventJuggler(DataOnDemand=True, Input=2.0, Output=4.0)

# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

# Get the configuration dictionary from the database
config = strippingConfiguration(stripping)
# Get the line builders from the archive
archive = strippingArchive(stripping)
# Build the streams
streams = buildStreams(stripping=config, archive=archive)


mystream = StrippingStream('Bs2KSKS.Strip')
mylines = ['StrippingBs2KSKS_DD_Run2_Line',
           'StrippingBs2KSKS_LD_Run2_Line',
           'StrippingBs2KSKS_LL_Run2_Line']

for stream in streams:
    if stream.name() == 'Bhadron':
        for line in stream.lines:
            if line.name() in mylines:
                line._prescale = 1.0
                mystream.appendLines([line])

sc = StrippingConf(Streams=[mystream],
                   MaxCandidates=2000,
                   MaxCombinations=int(1e7),
                   TESPrefix='Strip')

mystream.sequence().IgnoreFilterPassed = False


# Configure DST writer
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements)

SelDSTWriterElements = {
        'default': stripMicroDSTElements(pack=enablePacking, isMC=True)
}

SelDSTWriterConf = {
        'default': stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
}


dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Filtered',
                         SelectionSequences=sc.activeStreams())

# Add stripping TCK
from Configurables import StrippingTCK, StrippingReport
stck = StrippingTCK(HDRLocation='/Event/Strip/Phys/DecReports',
                    TCK=0x42722920)
sr = StrippingReport(Selections=sc.selections())

# Configure DaVinci
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence([sc.sequence()])
DaVinci().appendToMainSequence([sr])
DaVinci().appendToMainSequence([stck])
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = "Stripping"
DaVinci().DataType = "2017"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
