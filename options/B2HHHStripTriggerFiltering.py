"""
Stripping Filtering file for Chris Thomas
@author Andre Massafferri 
@date   2014-05-21
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping20'
config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)

def quickBuild(streamName):
    '''wrap buildStream to reduce typing'''
    return buildStream(stripping=config, streamName=streamName, archive=archive)

streams = []

_filterlines = quickBuild('Bhadron')

MyStream = StrippingStream("B2Kpipi.StripTrigger")
MyLines = ['StrippingB2hhh_KKK_inclLine', 'StrippingB2hhh_pph_inclLine' ]

for line in _filterlines.lines :
    if line.name() in MyLines:
        MyStream.appendLines( [ line ] ) 

for line in _filterlines.lines:
   line._prescale = 1.0
    
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = False 

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }

SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )

from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (

      HLT_Code   = "(HLT_PASS_RE('Hlt1TrackAllL0Decision') & HLT_PASS_RE('Hlt2Topo[234]Body.*Decision'))" 
    
   )

from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2012"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )

DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')
