"""
Filtering script for Bd -> pi+ pi- K+ pi-
Year: 2012
"""

stripping = 'stripping21r0p1' 

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

# copy the algorithm form the stripping line, without PID cuts (avoid bias)
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from GaudiConfUtils.ConfigurableGenerators import CombineParticles, FilterDesktop

#=========================================#
#===== Stripping line adjustment of ======#
#==============  B2KsthhLine =============#
#=========================================#

#copied algorithm from the stripping line, removing PID cuts (contained in StdVeryLooseDetatchedKst2Kpi and StdLoosePions) to avoid bias

def makeKst2Kpi( name, config ) :
    # define all the cuts
    _trkChi2Cut1      = "(CHILDCUT((TRCHI2DOF<%s),1))"   % config['Trk_Chi2']
    _trkChi2Cut2      = "(CHILDCUT((TRCHI2DOF<%s),2))"   % config['Trk_Chi2']
    _trkGhostProbCut1 = "(CHILDCUT((TRGHOSTPROB<%s),1))" % config['Trk_GhostProb']
    _trkGhostProbCut2 = "(CHILDCUT((TRGHOSTPROB<%s),2))" % config['Trk_GhostProb']

    _allCuts  = _trkChi2Cut1
    _allCuts += '&'+_trkChi2Cut2
    _allCuts += '&'+_trkGhostProbCut1
    _allCuts += '&'+_trkGhostProbCut2

    # make the filter
    _filterKst2Kpi = FilterDesktop( Code = _allCuts )
    
    # needs adjusting for changes made due to use of StdLoosePions and Kaons

    #=== StdVeryLooseDetachedKst2Kpi ===#

    # copied source code for this in order to remove PID cuts on pions and kaons included
    _kaons =  DataOnDemand('Phys/StdNoPIDsKaons/Particles')
    _pions =  DataOnDemand('Phys/StdNoPIDsPions/Particles')
    
    _StdVeryLooseDetachedKst2KpiRemovedPIDs = CombineParticles()
    _StdVeryLooseDetachedKst2KpiRemovedPIDs.DecayDescriptor = "[K*(892)0 -> K+ pi-]cc"
    
    ''' 
    # IPCHI2>4 already applied on StdLoose{Kaons,Pions} 
    StdVeryLooseDetachedKst2Kpi.DaughtersCuts = {
         "K+" :"(MIPCHI2DV(PRIMARY)>2.25)",
         "pi-":"(MIPCHI2DV(PRIMARY)>2.25)"
         }
    '''

    _StdVeryLooseDetachedKst2KpiRemovedPIDs.CombinationCut = "(ADAMASS('K*(892)0')<300*MeV) & (ADOCACHI2CUT(30, ''))"
    _StdVeryLooseDetachedKst2KpiRemovedPIDs.MotherCut = "(VFASPF(VCHI2)<25)"
    
    # make this a selection
    _stdKst2Kpi = Selection( 'DetachedKst2Kpi_noPIDs', Algorithm = _StdVeryLooseDetachedKst2KpiRemovedPIDs, RequiredSelections = [_kaons, _pions])

    # return the Selection object
    return Selection( name, Algorithm = _filterKst2Kpi, RequiredSelections = [_stdKst2Kpi] ) 


def makeB2Ksthh( name, Kst_sel, config ) :

    #redefine to StdNoPIDsPions from stripping line
    _pions = DataOnDemand(Location="Phys/StdNoPIDsPions/Particles")

    _trkChi2Cut      = "(TRCHI2DOF<%s)"   % config['Trk_Chi2']
    _trkGhostProbCut = "(TRGHOSTPROB<%s)" % config['Trk_GhostProb']

    _daughtersCuts = _trkChi2Cut
    _daughtersCuts += '&'+_trkGhostProbCut

    _massCutLow       = "(AM>(5279-%s)*MeV)"                                                        % config['B_Mlow']
    _massCutHigh      = "(AM<(5279+%s)*MeV)"                                                        % config['B_Mhigh']
    _aptCut           = "(APT>%s*MeV)"                                                              % config['B_APTmin']
    _daugMedPtCut     = "(ANUM(PT>%s*MeV)>=2)"                                                      % config['BDaug_MedPT_PT']
    _daugMaxPtIPCut   = "(AVAL_MAX(MIPDV(PRIMARY),PT)>%s)"                                          % config['BDaug_MaxPT_IP']
    _daugPtSumCut     = "((APT1+APT2+APT3)>%s*MeV)"                                                 % config['BDaug_PTsum']

    _combCuts = _aptCut
    _combCuts += '&'+_daugPtSumCut
    _combCuts += '&'+_daugMedPtCut
    _combCuts += '&'+_massCutLow
    _combCuts += '&'+_massCutHigh
    #_combCuts += '&'+_daugMaxPtIPCut

    _ptCut            = "(PT>%s*MeV)"                                                               % config['B_PTmin']
    _vtxChi2Cut       = "(VFASPF(VCHI2)<%s)"                                                        % config['B_VtxChi2']
    _diraCut          = "(BPVDIRA>%s)"                                                              % config['B_Dira']
    _ipChi2Cut        = "(MIPCHI2DV(PRIMARY)<%s)"                                                   % config['B_IPCHI2wrtPV']
    _fdCut            = "(VFASPF(VMINVDDV(PRIMARY))>%s)"                                            % config['B_FDwrtPV']
    _fdChi2Cut        = "(BPVVDCHI2>%s)"                                                            % config['B_FDChi2']
    _ipChi2SumCut     = "(SUMTREE(MIPCHI2DV(PRIMARY),((ABSID=='pi+') | (ABSID=='pi-')),0.0) > %s)"  % config['B_IPCHI2sum']

    _motherCuts = _ptCut
    _motherCuts += '&'+_vtxChi2Cut
    _motherCuts += '&'+_diraCut
    _motherCuts += '&'+_ipChi2Cut
    _motherCuts += '&'+_fdCut
    _motherCuts += '&'+_fdChi2Cut
    _motherCuts += '&'+_ipChi2SumCut

    _B = CombineParticles()
    _B.DaughtersCuts = { "pi+" : _daughtersCuts }
    _B.CombinationCut = _combCuts
    _B.MotherCut = _motherCuts

    _B.DecayDescriptors = [ "[B0 -> pi+ pi- K*(892)0]cc" ]
        
    return Selection (name, Algorithm = _B, RequiredSelections = [ Kst_sel, _pions ])

#================================#
#========= Selections ===========#
#================================#

#match these to config in stripping line 
config = {
     'Trk_Chi2'                : 4.0,
     'Trk_GhostProb'           : 0.4,
     #'Kstar_MassLo'            : 0.0,
     #'Kstar_MassHi'            : 5000.0,
     'B_Mlow'                  : 1279.0,
     'B_Mhigh'                 : 921.0,
     'B_APTmin'                : 1000.0,
     'B_PTmin'                 : 1500.0,
     'BDaug_MedPT_PT'          : 800.0,
     'BDaug_MaxPT_IP'          : 0.05,
     'BDaug_PTsum'             : 3000.0,
     'B_IPCHI2sum'             : 50.0,
     'B_VtxChi2'               : 12.0,
     'B_Dira'                  : 0.999,
     'B_IPCHI2wrtPV'           : 8.0,
     'B_FDwrtPV'               : 1.0,
     'B_FDChi2'                : 50.0,
     #'GEC_MaxTracks'           : 250,
     'ConeAngle10'             : 1.0, 
     'ConeAngle15'             : 1.5, 
     'ConeAngle17'             : 1.7, 
     #'HLT1Dec'                 : 'Hlt1(Two)?TrackMVADecision',
     #'HLT2Dec'                 : 'Hlt2Topo[234]BodyDecision',
}

#=== select K*0->K+ pi- ===#
selKst2Kpi = makeKst2Kpi(
        name = 'Kst_2Kpi_without_PID',
        config = config
        )
                    
#=== select B -> K* pi+ pi- ===#
selB2Ksthh = makeB2Ksthh(
        name = 'B2Ksthh_without_PID_for_B2RhoKst',
        Kst_sel = selKst2Kpi,
        config = config
        )
                 
relInfo    = [ { "Type" : "RelInfoConeVariables" 
              , "ConeAngle" : config['ConeAngle10']
              , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM'] 
              , 'Location'  : 'P2ConeVar10_B'
              , "DaughterLocations" : { "[B0 -> ^pi+ pi- (K*(892)0 -> K+ pi-)]CC" : 'P2ConeVar10_1',
                                        "[B0 -> pi+ ^pi- (K*(892)0 -> K+ pi-)]CC" : 'P2ConeVar10_2',
                                        "[B0 -> pi+ pi- (K*(892)0 -> ^K+ pi-)]CC" : 'P2ConeVar10_3',
                                        "[B0 -> pi+ pi- (K*(892)0 -> K+ ^pi-)]CC" : 'P2ConeVar10_4'} },
              { "Type" : "RelInfoConeVariables"
              , "ConeAngle" : config['ConeAngle15'] 
              , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM']
              , 'Location'  : 'P2ConeVar15_B'
              , "DaughterLocations" : { "[B0 -> ^pi+ pi- (K*(892)0 -> K+ pi-)]CC" : 'P2ConeVar15_1',
                                        "[B0 -> pi+ ^pi- (K*(892)0 -> K+ pi-)]CC" : 'P2ConeVar15_2',
                                        "[B0 -> pi+ pi- (K*(892)0 -> ^K+ pi-)]CC" : 'P2ConeVar15_3',
                                        "[B0 -> pi+ pi- (K*(892)0 -> K+ ^pi-)]CC" : 'P2ConeVar15_4'} },
              { "Type" : "RelInfoConeVariables"
              , "ConeAngle" : config['ConeAngle17'] 
              , "Variables" : ['CONEANGLE', 'CONEMULT', 'CONEPTASYM']
              , 'Location'  : 'P2ConeVar17_B'
              , "DaughterLocations" : { "[B0 -> ^pi+ pi- (K*(892)0 -> K+ pi-)]CC" : 'P2ConeVar17_1',
                                        "[B0 -> pi+ ^pi- (K*(892)0 -> K+ pi-)]CC" : 'P2ConeVar17_2',
                                        "[B0 -> pi+ pi- (K*(892)0 -> ^K+ pi-)]CC" : 'P2ConeVar17_3',
                                        "[B0 -> pi+ pi- (K*(892)0 -> K+ ^pi-)]CC" : 'P2ConeVar17_4'} },
              { "Type"      : "RelInfoVertexIsolation"
              , "Location"  : 'VtxIsolationVar'}
             ]


#### END OF SELECTION ALG

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream

MyStreams = StrippingStream("Bd2Kpipipi.StripTrig")

line = StrippingLine("B2KsthhLine",
                     #prescale  = config['Prescale'],
                     #postscale = config['Postscale'],
                     selection = selB2Ksthh,
                     #HLT1 = Hlt1Filter,
                     #HLT2 = Hlt2Filter,
                     #FILTER = GECCode, 
                     RelatedInfoTools = relInfo, 
                     MDSTFlag = True
                     )


MyStreams.appendLines( [line] )

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf(
        Streams = [MyStreams],
        MaxCandidates = 2000,
        MaxCombinations = 10000000, 
        AcceptBadEvents = False, 
        BadEventSelection = filterBadEvents, 
        TESPrefix = 'Strip'
    )

MyStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf 
                                      )


SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
    }

#Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
 
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix = '000000',
                          SelectionSequences = sc.activeStreams()
                          )

#include L0 trigger filtering
from PhysConf.Filters import LoKi_Filters
trigfltrs = LoKi_Filters (
    L0DU_Code = "L0_CHANNEL_RE('Muon|DiMuon|Electron|Photon|Hadron')",
    )

from Configurables import StrippingTCK
#TCK IN THE FOLLOWING FORMAT: DaVinci version (XXrXpX) Stripping version (XXrXpX) (i.e. DaVinci 41r4p5 with Stripping28r1p1 -> TCK:41452811)
#you can find the associated DaVinci version for each stripping in: https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x39112101)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().EventPreFilters = trigfltrs.filters('TrigFilters')

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

## TESTING
##################################################################
#DaVinci().DataType = "2012"
#DaVinci().DDDBtag   = "dddb-20170721-2 "
#DaVinci().CondDBtag = "sim-20160321-2-vc-md100"

#filtering rejection stats
#from Configurables import DumpFSR, DaVinci
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr_neutral.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

##################################################################
#from GaudiConf import IOHelper

# Use the local input data
#IOHelper().inputFiles(['00174026_00000416_5.AllStreams.dst'
#], clear=True)
    
