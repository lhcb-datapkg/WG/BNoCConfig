"""
Stripping Filtering file for Chris Thomas
@author Valdir Salustino
@date   2017-08-29
"""
#stripping version
#stripping='stripping34r0p2'
stripping='stripping34'

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)
 
from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"


from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False


#Fix for TrackEff lines
#
#from Configurables import DecodeRawEvent
#DecodeRawEvent().setProp("OverrideInputs",4.3)
#

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream, buildStream
from StrippingArchive import strippingArchive

config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)



streams = buildStreams(stripping = config, archive = archive)
MyStream = StrippingStream("B2HHH.Strip")
MyLines = ['StrippingBu2hhh_KKK_inclLine','StrippingBu2hhh_pph_inclLine','StrippingXb2phhLine','StrippingBu2hhh_KKK_samesignLine']
for stream in streams:
    if 'Bhadron' in stream.name() or 'Dimuon' in stream.name() or 'Leptonic' in stream.name():
        for line in stream.lines:
            if not line.name() in MyLines: continue
            dup=False
            for line2 in MyStream.lines:
                if line2.name()==line.name():
                    dup=True
                    break
            if not dup:
                line._prescale = 1.0
                MyStream.appendLines([line]) 

sc = StrippingConf( Streams = [MyStream],
                    MaxCandidates = 2000, MaxCombinations = 10000000,
                    TESPrefix = 'Strip' )

MyStream.sequence().IgnoreFilterPassed = False



enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,
                                                 selectiveRawEvent=True,
                                                 fileExtension='.dst')
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )



# Add stripping TCK 
#TCK IN THE FOLLOWING FORMAT: DaVinci version (XXrXpX) Stripping version (XXrXpX) (i.e. DaVinci 41r4p5 with Stripping28r1p1 -> TCK:41452811)
#you can find the associated DaVinci version for each stripping in: https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44403400)




from Configurables import DaVinci
#DaVinci().DataType = "2018"
DaVinci().EvtMax = -1 
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
 
# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

'''
files = []
import glob, os
from GaudiConf import IOHelper
folder = "/publicfs/lhcb/user/yangyh/MC_request/data/"
##os.chdir(folder)

for file in glob.glob(folder+"KKK/2012/*.dst"):
    files += [file]

IOHelper().inputFiles(files, clear=True)
'''

