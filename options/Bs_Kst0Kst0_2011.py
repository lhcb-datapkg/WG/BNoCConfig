"""
Filtering script for Bs -> ( K*0(892) -> K+ pi- ) ( K*0~(892) -> K- pi+ )
Year: 2011
"""

# copy the algorithm form the stripping line, without PID cuts (avoid bias)
from PhysSelPython.Wrappers import Selection, DataOnDemand
from StrippingConf.StrippingLine import StrippingLine
from GaudiConfUtils.ConfigurableGenerators import CombineParticles

def makeKst_02Kpi(
        name, KaonPT, KaonIPCHI2,PionPT,
        PionIPCHI2, KstarPT, KstarAPT,
        KstarVCHI2, KstarMassWin_ul, MaxGHOSTPROB):


    _stdKaons = DataOnDemand(Location="Phys/StdNoPIDsKaons/Particles")
    _stdPions = DataOnDemand(Location="Phys/StdNoPIDsPions/Particles")


    _Kstar_02Kpi = CombineParticles()

    _Kstar_02Kpi.DecayDescriptor = "[K*(892)0 -> K+ pi-]cc"
    _Kstar_02Kpi.DaughtersCuts = {"K+" : "(PT > %(KaonPT)s *MeV) & (MIPCHI2DV(PRIMARY)> %(KaonIPCHI2)s) & (TRGHOSTPROB < %(MaxGHOSTPROB)s)"% locals()
                                ,"pi-" : "(PT > %(PionPT)s *MeV) & (MIPCHI2DV(PRIMARY)> %(PionIPCHI2)s) & (TRGHOSTPROB < %(MaxGHOSTPROB)s)"% locals()}
                                
    _Kstar_02Kpi.CombinationCut = "(AM < %(KstarMassWin_ul)s *MeV) & (APT > %(KstarAPT)s *MeV)"% locals()
    _Kstar_02Kpi.MotherCut = "(VFASPF(VCHI2/VDOF)< %(KstarVCHI2)s) & (PT > %(KstarPT)s *MeV)"% locals()



    return Selection(name, Algorithm = _Kstar_02Kpi, RequiredSelections = [_stdKaons,_stdPions])

def makeBs2Kst_0Kst_0(
        name, Kst_0sel, BMassWin, BVCHI2,
        BDOCA, BIPCHI2, BFDistanceCHI2,
        SumPT,BDIRA):

    _motherCuts = " (VFASPF(VCHI2/VDOF) < %(BVCHI2)s) & (MIPCHI2DV(PRIMARY)< %(BIPCHI2)s) & (BPVVDCHI2 > %(BFDistanceCHI2)s) & (BPVDIRA > %(BDIRA)s)"% locals()
    _combinationCut = "(ADAMASS('B_s0') < %(BMassWin)s *MeV) & (AMAXDOCA('',False)< %(BDOCA)s *mm) "\
                        "& ( (AMINCHILD(PT,ID=='K+') + AMINCHILD(PT,ID=='K-') + AMINCHILD(PT,ID=='pi-') + AMINCHILD(PT,ID=='pi+'))> %(SumPT)s *MeV)" % locals() 

    _Bs = CombineParticles()
    _Bs.DecayDescriptor = "B_s0 -> K*(892)0 K*(892)~0"
    _Bs.CombinationCut = _combinationCut
    _Bs.MotherCut = _motherCuts

    _Bs.ReFitPVs = True

    return Selection(name, Algorithm = _Bs, RequiredSelections = [Kst_0sel])

# select K*0 -> K+ pi-
selKst_02Kpi = makeKst_02Kpi(
                name = 'Kst_02Kpi_without_PID_ForBs2K0stK0st',
                KaonPT = 500.0,
                KaonIPCHI2 = 9.0,
                PionPT = 500.0,
                PionIPCHI2 = 9.0,
                KstarPT = 900.0,
                KstarAPT = 800.0,
                KstarVCHI2 = 9.0,
                KstarMassWin_ul = 1600.0,
                MaxGHOSTPROB = 0.8)

# select Bs -> K*0 K*0~
selBs2Kst_0Kst_0 = makeBs2Kst_0Kst_0(
                    name = 'Bs2K0stK0st',
                    Kst_0sel = selKst_02Kpi,
                    BMassWin = 500.0,
                    BVCHI2 = 15.0,
                    BDOCA = 0.3,
                    BIPCHI2 = 25.0,
                    BFDistanceCHI2 = 81.0,
                    SumPT = 5000.0,
                    BDIRA = 0.99)



from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

# Build the streams and stripping object

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive



MyStreams = StrippingStream("Bs2K0stK0st.Strip")

line = StrippingLine(
	'Bs2K0stK0stNominalLine',
	prescale = 1,
	postscale = 1,
	algos = [ selBs2Kst_0Kst_0 ],
	EnableFlavourTagging = True,
	MDSTFlag = False)

MyStreams.appendLines( [line] )

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf(
        Streams = [MyStreams],
        MaxCandidates = 2000,
        MaxCombinations = 10000000,
	      AcceptBadEvents = False,
	      BadEventSelection = filterBadEvents,
        TESPrefix = 'Strip'
    )

MyStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default': stripDSTElements(
                            pack=enablePacking
                            )
    }

SelDSTWriterConf = {
    'default': stripDSTStreamConf(
                            pack=enablePacking,
                            selectiveRawEvent=True,
                            fileExtension='.dst'
                            )
    }
#Items that get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().DataType = "2011"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
