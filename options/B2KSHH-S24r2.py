"""
Options for building Stripping24r2.
"""

#use CommonParticlesArchive
stripping='stripping24r2'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.3)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

MyStream = StrippingStream("B2KSHH.Strip")
MyLines = ['StrippingB2KShh_DD_Run2_SS_Line', 'StrippingB2KShh_LD_Run2_OS_Line', 'StrippingB2KShh_LD_Run2_SS_Line', 'StrippingB2KShh_LL_Run2_OS_Line',
 'StrippingB2KShh_LL_Run2_SS_Line', 'StrippingB2KShh_DD_Run2_OS_Line']
for s in streams :
    if  s.name()=="Bhadron" or s.name()=="BhadronCompleteEvent":
     for line in s.lines :
        if line.name() in MyLines:
            line._HLT1 = None
            line._HLT2 = None
            MyStream.appendLines( [ line ] )

     for line in s.lines:
       s._prescale = 1.0



from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [MyStream],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents,
                    #MaxCombinations = 10000000,
                    TESPrefix = 'Strip' )

MyStream.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=True)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44105242)

# Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = ["/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99"]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().DataType = "2015"
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
