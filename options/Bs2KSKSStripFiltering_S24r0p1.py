"""
Stripping Filtering file for Bs->KSKS, Stripping 24r0p1
@author Moritz Demmer, Timon Schmelzer
@date   2017-06-06

"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
#from CommonParticles.Utils import DefaultTrackingCuts
#DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
#                                "CloneDistCut" : [5000, 9e+99 ] }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
# from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
# from StrippingArchive import strippingArchive

# stripping='stripping21'
# #get the configuration dictionary from the database
# config  = strippingConfiguration(stripping)
# #get the line builders from the archive
# archive = strippingArchive(stripping)

# streams = buildStreams(stripping = config, streamName='Bhadron', archive = archive)

from StrippingArchive.Stripping24r0p1.StrippingBnoC.StrippingBs2KSKS_Run2 import Bs2KSKSConf as builder
from StrippingArchive.Stripping24r0p1.StrippingBnoC.StrippingBs2KSKS_Run2 import default_config

stream = StrippingStream("BsKSKS.Strip")

lb = builder(name=default_config['NAME'], config=default_config['CONFIG'])
MyLines = [ 'StrippingBs2KSKS_LL_Run2_Line', 'StrippingBs2KSKS_LD_Run2_Line', 'StrippingBs2KSKS_DD_Run2_Line' ]
# stream.appendLines( lb.lines() )
for line in lb.lines():
  if line.name() in MyLines:
    print 'MyLines entry', line.name()
    stream.appendLines( [ line ] )

# turn off all pre-scalings 
for line in stream.lines:
  line._prescale = 1.0 

sc = StrippingConf( Streams = [ stream ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

stream.sequence().IgnoreFilterPassed = False

#
# Configuration of SelDSTWriter
#
enablePacking = True
#enablePacking = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x38142401)

from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections())

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ sr ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# for TESTING needed only:
#DaVinci().DataType = "2016"

# Change the column size of Timing table
#from Configurables import TimingAuditor, SequencerTimerTool
#TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
#TimingAuditor().TIMER.NameSize = 60

