"""
Filtering script for B+ -> Ks pi- pi+ pi-
Year: 2016
"""

#stripping version
stripping='stripping28r2' 

#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)
 
from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"
 
#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider 
TrackStateProvider().CacheStatesOnDemand = False 

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams #,cloneLinesFromStream
from StrippingArchive import strippingArchive
 
 
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)
 
streams = buildStreams(stripping = config, archive = archive)

MyStreams = StrippingStream("Bu2Kspipipi.Strip")			 

linesToAdd = []

MyLines = [ 'StrippingBu2KsthhDDLine',
            'StrippingBu2KsthhLLLine'
            ]


for stream in streams:
    if 'Bhadron' in stream.name():
        for line in stream.lines:
            line._prescale = 1.0
            if line.name() in MyLines:
                linesToAdd.append(line)
MyStreams.appendLines(linesToAdd)
 
 
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStreams ],
                    MaxCandidates = 2000,
                    MaxCombinations = 10000000, 
                    AcceptBadEvents = False, 
                    BadEventSelection = filterBadEvents, 
                    TESPrefix = 'Strip'
                    )
 
 
MyStreams.sequence().IgnoreFilterPassed = False # so that we do not get all events written out
 

enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements,
                                      stripMicroDSTStreamConf,
                                      stripMicroDSTElements,
                                      stripCalibMicroDSTStreamConf 
                                      )

SelDSTWriterElements = {
    'default'               : stripMicroDSTElements(pack=enablePacking,isMC=True)
    }

SelDSTWriterConf = {
    'default'               : stripMicroDSTStreamConf(pack=enablePacking, isMC=True)
    }


##Items that might get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
 
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs
 
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )
 
# Add stripping TCK
from Configurables import StrippingTCK

#TCK IN THE FOLLOWING FORMAT: DaVinci version (XXrXpX) Stripping version (XXrXpX) (i.e. DaVinci 41r4p5 with Stripping28r1p1 -> TCK:41452811)
#you can find the associated DaVinci version for each stripping in: https://twiki.cern.ch/twiki/bin/view/Main/ProcessingPasses
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44105282)

from Configurables import DaVinci

#
# DaVinci Configuration
#
DaVinci().Simulation = True
DaVinci().EvtMax = -1  # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60


## TESTING
##################################################################
#DaVinci().DataType = "2016"
#DaVinci().DDDBtag   = "dddb-20170721-3"
#DaVinci().CondDBtag = "sim-20170721-2-vc-md100"

#filtering rejection stats
#from Configurables import DumpFSR, DaVinci
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr_charged_mode.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]
##################################################################

#from GaudiConf import IOHelper

# Use the local input data
#IOHelper().inputFiles([
#    '00191763_00000303_7.AllStreams.dst'
#], clear=True)
