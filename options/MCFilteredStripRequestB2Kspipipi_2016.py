"""
Options for building Stripping28r1.
"""

#use CommonParticlesArchive
stripping='stripping28r1'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

AllStreams = StrippingStream("B2KShhh.Strip")
MyLines = ['StrippingB2KShhh_PiPiPi_DDLine',
           'StrippingB2KShhh_PiPiPi_LLLine']

# These lines are needed to remove duplicates (everything goes to the same stream)
for stream in streams:
    if 'Bhadron' in stream.name() or 'Dimuon' in stream.name() or 'Leptonic' in stream.name():
        for line in stream.lines:
            if not line.name() in MyLines: continue
            dup=False
            for line2 in AllStreams.lines:
                if line2.name()==line.name():
                    dup=True
                    break
            if not dup:
                line._prescale = 1.0
                AllStreams.appendLines([line])

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000, MaxCombinations = 10000000,
                    TESPrefix = 'Strip' )

AllStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,
                                                 selectiveRawEvent=True,
                                                 fileExtension='.dst')
    }
#Items that get lost when running the CALO+PROTO ReProcessing in DV
caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
# Make sure they are present on full DST streams
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41442810)

#
# DaVinci Configuration
#
from Configurables import DaVinci
#DaVinci().DataType = "2016"
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
'''
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20170721-2-vc-md100"
'''
# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
'''
files = []
import glob, os
from GaudiConf import IOHelper
folder = '/scratch27/pablo.baladron/results_jobs2/'
#os.chdir(folder)
for file in glob.glob(folder+"*/Brunel-12105151.dst"):
    files += [file]

IOHelper().inputFiles(files, clear=True)

'''
