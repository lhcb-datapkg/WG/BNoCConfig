"""
Stripping Filtering file for Bu->KSKSh, 2012
@author Moritz Demmer, Timon Schmelzer
@date   2017-05-04

"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
#from CommonParticles.Utils import DefaultTrackingCuts
#DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
#                                "CloneDistCut" : [5000, 9e+99 ] }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
# from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
# from StrippingArchive import strippingArchive

# stripping='stripping21'
# #get the configuration dictionary from the database
# config  = strippingConfiguration(stripping)
# #get the line builders from the archive
# archive = strippingArchive(stripping)

# streams = buildStreams(stripping = config, streamName='Bhadron', archive = archive)

from StrippingArchive.Stripping21.StrippingHb2V0V0h import Hb2V0V0hConf as builder
from StrippingArchive.Stripping21.StrippingHb2V0V0h import default_config

stream = StrippingStream("Bu2KSKSh.Strip")

lb = builder(name='Hb2V0V0h', config=default_config['CONFIG'])
MyLines = [ 'StrippingHb2V0V0h_KSKShLL_Line', 'StrippingHb2V0V0h_KSKShLD_Line', 'StrippingHb2V0V0h_KSKShDD_Line' ]
# stream.appendLines( lb.lines() )
for line in lb.lines():
  if line.name() in MyLines:
    print 'MyLines entry', line.name()
    stream.appendLines( [ line ] )

# turn off all pre-scalings 
for line in stream.lines:
  line._prescale = 1.0 

from Configurables import  ProcStatusCheck
filterBadEvents =  ProcStatusCheck()

# Configure the stripping
sc = StrippingConf( Streams = [ stream ],
                   MaxCandidates = 2000,
                   AcceptBadEvents = False,
                   BadEventSelection = filterBadEvents )

stream.sequence().IgnoreFilterPassed = False

#
# Configuration of SelDSTWriter
#
enablePacking = True
#enablePacking = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf()#pack=enablePacking
    }

stripElements = stripDSTElements()#pack=enablePacking

SelDSTWriterElements = {
    'default'               : stripElements
    }

# # Items that might get lost when running the CALO+PROTO ReProcessing in DV
# caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]

# # Make sure they are present on full DST streams
# SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36142100)

from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections())

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ sr ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# for TESTING needed only:
DaVinci().DataType = "2012"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

