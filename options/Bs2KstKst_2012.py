"""
Stripping script for Bs -> ( K*0(892) -> K+ pi- ) ( K*0~(892) -> K- pi+ )
Year: 2012
Stripping version: 21r0p2
"""

#use CommonParticlesArchive
stripping = 'stripping21r0p2'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

# Build the streams and stripping object

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)

#get the line builders from the archive
archive = strippingArchive(stripping)

# build the streams
streams = buildStreams(stripping = config, archive = archive)

MyStreams = StrippingStream("Bs2K0stK0st.Strip")
myLine = 'StrippingBs2K0stK0stNominalLine'

# These lines are needed to remove duplicates (everything goes to the same stream)
for s in streams :
    if s.name()=='BhadronCompleteEvent':
        for line in s.lines :
            if line.name() == myLine:
                MyStreams.appendLines( [ line ] )

sc = StrippingConf(
        Streams = [MyStreams],
        MaxCandidates = 2000,
        MaxCombinations = 10000000,
        TESPrefix = 'Strip'
    )

MyStreams.sequence().IgnoreFilterPassed = False

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default': stripDSTElements(
                            pack=enablePacking
                            )
    }

SelDSTWriterConf = {
    'default': stripDSTStreamConf(
                            pack=enablePacking,
                            selectiveRawEvent=True,
                            fileExtension='.dst'
                            )
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(
        HDRLocation = '/Event/Strip/Phys/DecReports',
        TCK=0x39112101
    )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
DaVinci().DataType = "2012"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60