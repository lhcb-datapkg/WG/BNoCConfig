"""
@author Cameron Dean
@date   2017-03-06
"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

stripping='stripping28'
config  = strippingConfiguration(stripping)
config = {'Hb2Charged2Body' : config['Hb2Charged2Body'], 'B2HHBDT' : config['B2HHBDT']}
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)

MyStream = StrippingStream("B2hh.Strip")
MyLines = ['StrippingHb2Charged2BodyB2Charged2BodyLine', 'StrippingB2HHBDTLine']

for line in streams :
    if line.name() in MyLines:
        MyStream.appendLines( [ line ] ) 

for line in streams:
   line._prescale = 1.0
    
from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

MyStream.sequence().IgnoreFilterPassed = False 

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                              stripDSTStreamConf,
                                              stripDSTElements
                                              )
SelDSTWriterElements = {
    'default'              : stripDSTElements()
    }

SelDSTWriterConf = {
    'default'              : stripDSTStreamConf()
    }

for stream in sc.activeStreams() :
   print "there is a stream called " + stream.name() + " active"

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='',
                          SelectionSequences = sc.activeStreams()
                          )

from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x41412800)

from Configurables import DaVinci
DaVinci().InputType = 'DST'
DaVinci().DataType = "2016"
DaVinci().Simulation = True
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
