"""
Stripping Filtering file for B0->PhiKst0
@author Anh-Duc Nguyen
@date   2013-09-29

"""

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

# Tighten Trk Chi2 to <3
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 3 ],
                                "CloneDistCut" : [5000, 9e+99 ] }

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive
#
import os, sys, re


stripping='stripping20'
#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)


#
# Merge into one stream and run in flag mode
#
#AllStreams = StrippingStream("AllStreams")
MyStream = StrippingStream("PhiKst0.Strip")
MyLines = ['StrippingBetaSBs2PhiKstNominalLine', 'StrippingBsPhiRhoLine', 'StrippingBetaSBs2Q2Body4piLine']

for stream in streams:
    for line in stream.lines:
        if line.name() in MyLines:
            print 'MyLines entry', line.name()
            MyStream.appendLines( [ line ] )
        #MyStream.appendLines(stream.lines)

# turn off all pre-scalings 
for stream in streams: 
    for line in stream.lines:
        line._prescale = 1.0 

sc = StrippingConf( Streams = [ MyStream ],
                    MaxCandidates = 2000,
                    TESPrefix = 'Strip'
                    )

MyStream.sequence().IgnoreFilterPassed = False

#
# Configuration of SelDSTWriter
#
enablePacking = True
#enablePacking = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking)
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x32210200)


#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"
#
# for TESTING needed only:
#DaVinci().DataType = "2012"
#
#from Configurables import DumpFSR, DaVinci
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

#from GaudiConf import IOHelper
#IOHelper('ROOT').inputFiles(['Brunel-11104022.dst'], clear=True)
